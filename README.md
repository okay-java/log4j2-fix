# log4j2-fix

Log4j2 Fix - Log4j2 Vulnerability fix in core java project.

#Log4j2.properties file
#Log4j2 configuration
status = debug
name = log4j2Config

#Log file location and name
property.basePath = /app/demo/
property.filename=springapp.log

#Rolling File appender
appender.rolling.type = RollingFile
appender.rolling.name = RollingFileAppender
appender.rolling.fileName =  ${basePath}${filename}
appender.rolling.filePattern = ${filename}.%d{yyy-MM-dd}
appender.rolling.layout.type = PatternLayout
appender.rolling.layout.pattern = [%d] %t %c %L %-5p  - %m%n

#Time based log file rotation policy
appender.rolling.policies.type = Policies
appender.rolling.policies.time.type = TimeBasedTriggeringPolicy
appender.rolling.policies.time.interval = 1
appender.rolling.policies.time.modulate = true
appender.rolling.strategy.type = DefaultRolloverStrategy

# Must configure root logger
rootLogger.level = debug
rootLogger.appenderRef.rolling.ref = RollingFileAppender

## Getting started

What is Log4j2 VUNERABILITY or 0 day vulnerability?

JNDI – Look up
The Java Naming and Directory Interface (JNDI) is a Java API for a directory service that allows Java software clients to discover and look up data and resources (in the form of Java objects) via a name.. database/Ldap server etc
Attackers can use the JNDI look up to install any exe file or run a shell script on your production server.
${jndi:ldap://{malicious website}/a}
${jndi:ldap:/}${jndi:ldaps:/}
${jndi:rmi:/}
${jndi:dns:/}
${jndi:iiop:/}

Affected versions??
https://logging.apache.org/log4j/2.x/security.html
Log4j1.x  - safe and secure :) 
Apache Log4j 2.x to 2.15.0 :(
To be more specific log4j-x-core.jar is the impacted version.

FIX/Mitigation
Log4j1.x  - safe and secure :)
No action required…. Cheers !!
Apache Log4j 2.x to 2.15.0  ??
Java 8 (or later) users should upgrade to release 2.16.0.
Java 7 should upgrade to release 2.12.2 when it becomes available (WIP)
For gt;=2.10, set system property log4j2.formatMsgNoLookups to true.
For gt;=2.10, set environment variable LOG4J_FORMAT_MSG_NO_LOOKUPS to true.
Rmove the JndiLookup log4j-core-*.jar org/apache/logging/log4j/core/lookup/JndiLookup.class

Read more about the security vulnerability 
https://logging.apache.org/log4j/2.x/security.html
JIRA ticket
https://issues.apache.org/jira/browse/LOG4J2-3221
https://unit42.paloaltonetworks.com/apache-log4j-vulnerability-cve-2021-44228/
https://blog.talosintelligence.com/2021/12/apache-log4j-rce-vulnerability.html
https://www.trendmicro.com/en_us/research/21/l/patch-now-apache-log4j-vulnerability-called-log4shell-being-acti.html
https://www.apache.org/dyn/closer.lua/logging/log4j/2.16.0/apache-log4j-2.16.0-bin.zip
https://logging.apache.org/log4j/2.x/manual/configuration.html

subscribe okayjava..thank you
